package com.se121.creational.prototype;

// Prototype interface
interface Prototype {
    Prototype clone();
}

// Concrete Prototype
class ConcretePrototype implements Prototype {
    private String property;

    public ConcretePrototype(String property) {
        this.property = property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public Prototype clone() {
        return new ConcretePrototype(this.property);
    }

    public void display() {
        System.out.println("Property: " + property);
    }
}

// Client class
class Client {
    private Prototype prototype;

    public Client(Prototype prototype) {
        this.prototype = prototype;
    }

    public void operation() {
        Prototype clone = prototype.clone();
        ((ConcretePrototype) clone).setProperty("New Property");
        clone.display();
    }
}

// Main class
class Main {
    public static void main(String[] args) {
        Prototype prototype = new ConcretePrototype("Original Property");
        Client client = new Client(prototype);

        // Cloning and modifying the prototype
        client.operation();
    }
}
