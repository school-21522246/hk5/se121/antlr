package com.se121.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

// Flyweight interface
interface Flyweight {
    void operation(int extrinsicState);
}

// ConcreteFlyweight class
class ConcreteFlyweight implements Flyweight {
    private String intrinsicState;

    public ConcreteFlyweight(String intrinsicState) {
        this.intrinsicState = intrinsicState;
    }

    @Override
    public void operation(int extrinsicState) {
        System.out.println("ConcreteFlyweight with intrinsic state: " + intrinsicState +
                " and extrinsic state: " + extrinsicState);
    }
}
// UnsharedConcreteFlyweight class
class UnsharedConcreteFlyweight implements Flyweight {
    private String intrinsicState;

    public UnsharedConcreteFlyweight(String intrinsicState) {
        this.intrinsicState = intrinsicState;
    }

    @Override
    public void operation(int extrinsicState) {
        System.out.println("UnsharedConcreteFlyweight with intrinsic state: " + intrinsicState +
                " and extrinsic state: " + extrinsicState);
    }
}

// FlyweightFactory class
class FlyweightFactory {
    private Map<String, Flyweight> flyweights = new HashMap<>();

    public Flyweight getFlyweight(String key) {
        if (!flyweights.containsKey(key)) {
            if (key.equals("shared")) {
                flyweights.put(key, new ConcreteFlyweight("shared"));
            } else {
                return new UnsharedConcreteFlyweight("unshared");
            }
        }
        return flyweights.get(key);
    }
}

// Client class
public class Diagram {
    public static void main(String[] args) {
        FlyweightFactory factory = new FlyweightFactory();

        Flyweight flyweight1 = factory.getFlyweight("shared");
        flyweight1.operation(1);

        Flyweight flyweight2 = factory.getFlyweight("shared");
        flyweight2.operation(2);

        Flyweight flyweight3 = factory.getFlyweight("unshared");
        flyweight3.operation(3);
    }
}
