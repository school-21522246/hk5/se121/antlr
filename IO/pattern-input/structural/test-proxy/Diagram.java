package com.se121.structural.proxy;

// Subject interface
interface Subject {
    void request();
}

// RealSubject class
class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("RealSubject is handling the request");
    }
}

// Proxy class
class Proxy implements Subject {
    private RealSubject realSubject;

    @Override
    public void request() {
        // Create or obtain the RealSubject when needed
        if (realSubject == null) {
            realSubject = new RealSubject();
        }

        // Additional logic can be added here before or after delegating to the RealSubject
        System.out.println("Proxy is handling the request");

        // Delegating the request to the RealSubject
        realSubject.request();
    }
}

// Client class
public class Diagram {
    public static void main(String[] args) {
        Subject proxy = new Proxy();
        proxy.request();
    }
}
