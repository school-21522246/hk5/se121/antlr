package com.se121.structural.proxy;

import javafx.geometry.Point2D;

public class Main {

	public static void main(String[] args) {
		Image img = ImageFactory.getImage("A1.bmp");

		img.setLocation(new Point2D(1,1));
		System.out.println("Image location: " + img.getLocation());
		System.out.println("rendering image now...");
		img.render();
	}

}
