// Singleton class
public class Singleton {
    // Private static instance variable
    private static Singleton instance;

    // Private constructor to prevent external instantiation
    private Singleton() {
        // Initialization code, if any
    }

    // Public static method to retrieve or create the instance (Lazy Initialization)
    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    // Other methods and attributes can be added as needed
    public void showMessage() {
        System.out.println("Hello from Singleton!");
    }
}

// Client class
public class Client {
    public void useSingleton() {
        // Get the Singleton instance
        Singleton singleton = Singleton.getInstance();

        // Use the Singleton instance
        singleton.showMessage();
    }
}

// Main class
public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        client.useSingleton();
    }
}
