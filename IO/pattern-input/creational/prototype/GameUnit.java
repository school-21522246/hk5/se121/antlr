package com.se121.creational.prototype;
/**
 * This class represents an abstract prototype & defines the clone method
 */
public abstract class GameUnit {
	public abstract void reset();

    public GameUnit clone() {
		GameUnit clone = (GameUnit) super.clone();
		clone.initialize();
		return clone;
    }
	protected  void initialize() {
	}
}
