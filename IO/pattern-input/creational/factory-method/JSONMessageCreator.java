package com.se121.creational.factory.method;

import com.se121.creational.factory.method.message.JSONMessage;
import com.se121.creational.factory.method.message.Message;

/**
 * Provides implementation for creating JSON messages
 */
public class JSONMessageCreator extends MessageCreator {

    @Override
    public Message creatMessage() {
        return new JSONMessage();
    }
	
}
