package com.se121.creational.factory.method;

import com.se121.creational.factory.method.message.Message;

public class Main {

	public static void main(String[] args) {
		printMessage(new JSONMessageCreator());
		printMessage(new TextMessageCreator());
	}
	
	public static void printMessage(MessageCreator creator) {
		Message message = creator.getMessage();
		System.out.println(message.getContent());
	}
}
