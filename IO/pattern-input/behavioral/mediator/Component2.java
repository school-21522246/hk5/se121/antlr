package com.se121.behavioral.mediator;

public class Component2 extends BaseComponent{
    public void doSomethingC() {
        System.out.println("Component 2 does C.");
        this.mediator.notify(this, "C");
    }

    public void doSomethingD() {
        System.out.println("Component 2 does D.");
        this.mediator.notify(this, "D");
    }

}
