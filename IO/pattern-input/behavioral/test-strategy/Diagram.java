package com.se121.behavioral.strategy;

// Strategy interface
interface Strategy {
    void performOperation();
}

// ConcreteStrategyAdd class
class ConcreteStrategyAdd implements Strategy {
    @Override
    public void performOperation() {
        System.out.println("Performing addition operation");
    }
}

// ConcreteStrategySubtract class
class ConcreteStrategySubtract implements Strategy {
    @Override
    public void performOperation() {
        System.out.println("Performing subtraction operation");
    }
}

// Context class
class Context {
    private Strategy strategy;

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void executeOperation() {
        strategy.performOperation();
    }
}

// Client class
public class Client {
    public static void main(String[] args) {
        Context context = new Context();

        // Using ConcreteStrategyAdd
        context.setStrategy(new ConcreteStrategyAdd());
        context.executeOperation();

        // Using ConcreteStrategySubtract
        context.setStrategy(new ConcreteStrategySubtract());
        context.executeOperation();
    }
}
