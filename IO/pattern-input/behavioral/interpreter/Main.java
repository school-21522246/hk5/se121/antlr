package com.se121.behavioral.interpreter;

public class Main {

	public static void main(String[] args) {
		Report report1  = new Report("Cashflow repot", "FINANCE_ADMIN OR ADMIN AND USER");
		ExpressionBuilder builder = new ExpressionBuilder();
		
		PermissionExpression exp = builder.build(report1);
		System.out.println(exp);
		
		User user1 = new User("Dave", "USER", "ADMIN");
		
		System.out.println("USer access report:"+ exp.interpret(user1));
	}

}
