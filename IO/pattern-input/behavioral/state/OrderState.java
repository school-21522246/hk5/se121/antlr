package com.se121.behavioral.state;

//Abstract state
public interface OrderState {
    double handleCancellation();
}
