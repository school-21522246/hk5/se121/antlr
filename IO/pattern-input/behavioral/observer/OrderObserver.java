package com.se121.behavioral.observer;

//Abstract observer
public interface OrderObserver {

    void updated(Order order);
}
