package com.se121.behavioral.memento;

// Memento class
class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}

// Originator class
class Originator {
    private String state;

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public Memento saveStateToMemento() {
        return new Memento(state);
    }

    public void restoreStateFromMemento(Memento memento) {
        state = memento.getState();
    }
}

// Caretaker class
class Caretaker {
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}

// Client class
public class Client {
    public static void main(String[] args) {
        Originator originator = new Originator();
        Caretaker caretaker = new Caretaker();

        // Setting the state and saving it to a memento
        originator.setState("State1");
        caretaker.setMemento(originator.saveStateToMemento());

        // Changing the state
        originator.setState("State2");

        // Restoring the state from the memento
        originator.restoreStateFromMemento(caretaker.getMemento());

        System.out.println("Current State: " + originator.getState());
    }
}
