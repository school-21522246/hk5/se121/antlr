package com.se121.behavioral.state;

// State interface
interface State {
    void handle(Context context);
}

// ConcreteStateA class
class ConcreteStateA implements State {
    @Override
    public void handle(Context context) {
        System.out.println("Handling with ConcreteStateA");
        // Perform state-specific operations
        context.setState(new ConcreteStateB());
    }
}

// ConcreteStateB class
class ConcreteStateB implements State {
    @Override
    public void handle(Context context) {
        System.out.println("Handling with ConcreteStateB");
        // Perform state-specific operations
        context.setState(new ConcreteStateA());
    }
}

// Context class
class Context {
    private State state;

    public Context() {
        // Initial state
        this.state = new ConcreteStateA();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void request() {
        state.handle(this);
    }
}

// Client class
public class Client {
    public static void main(String[] args) {
        Context context = new Context();

        // Request with initial state ConcreteStateA
        context.request();

        // Request with updated state ConcreteStateB
        context.request();
    }
}
