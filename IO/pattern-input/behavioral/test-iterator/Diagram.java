package com.se121.behavioral.iterator;

public interface Iterator<T> {
    boolean hasNext();
    T next();
}
// ConcreteIterator class
class ConcreteIterator implements Iterator<Object> {
    private Object[] elements;
    private int position = 0;

    public ConcreteIterator(Object[] elements) {
        this.elements = elements;
    }

    @Override
    public boolean hasNext() {
        return position < elements.length;
    }

    @Override
    public Object next() {
        if (hasNext()) {
            return elements[position++];
        }
        return null;
    }
}

// Aggregate interface
interface Aggregate {
    Iterator<Object> createIterator();
}

// ConcreteAggregate class
class ConcreteAggregate implements Aggregate {
    private Object[] elements;

    public ConcreteAggregate(Object[] elements) {
        this.elements = elements;
    }

    @Override
    public Iterator<Object> createIterator() {
        return new ConcreteIterator(elements);
    }
}

// Client class
public class Client {
    public static void main(String[] args) {
        Object[] elements = { "A", "B", "C", "D" };

        Aggregate aggregate = new ConcreteAggregate(elements);
        Iterator iterator = aggregate.createIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
