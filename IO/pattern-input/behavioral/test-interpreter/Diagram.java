package com.se121.behavioral.interpreter;

// AbstractExpression interface
interface AbstractExpression {
    void interpret(Context context);
}

// TerminalExpression class
class TerminalExpression implements AbstractExpression {
    @Override
    public void interpret(Context context) {
        System.out.println("TerminalExpression is interpreting the context");
    }
}

// NonTerminalExpression class
class NonTerminalExpression implements AbstractExpression {
    private AbstractExpression expression;

    public NonTerminalExpression(AbstractExpression expression) {
        this.expression = expression;
    }

    @Override
    public void interpret(Context context) {
        System.out.println("NonTerminalExpression is interpreting the context");
        expression.interpret(context);
    }
}

// Context class
class Context {
    private String data;

    public Context(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}

// Client class
public class Diagram {
    public static void main(String[] args) {
        Context context = new Context("Interpreter Pattern");

        AbstractExpression expression = new NonTerminalExpression(new TerminalExpression());
        expression.interpret(context);
    }
}
