package com.se121.behavioral.template.method;

// AbstractClass
abstract class AbstractClass {
    // Template method
    public final void templateMethod() {
        commonOperation1();
        specificOperation();
        commonOperation2();
        hook(); // Optional hook method
    }

    // Common operation 1
    private void commonOperation1() {
        System.out.println("Performing common operation 1");
    }

    // Common operation 2
    private void commonOperation2() {
        System.out.println("Performing common operation 2");
    }

    // Abstract method to be implemented by subclasses
    protected abstract void specificOperation();

    // Hook method (optional and may be empty)
    protected void hook() {}
}

// ConcreteClass1
class ConcreteClass1 extends AbstractClass {
    @Override
    protected void specificOperation() {
        System.out.println("Performing specific operation for ConcreteClass1");
    }
}

// ConcreteClass2
class ConcreteClass2 extends AbstractClass {
    @Override
    protected void specificOperation() {
        System.out.println("Performing specific operation for ConcreteClass2");
    }

    @Override
    protected void hook() {
        System.out.println("ConcreteClass2 is providing an optional hook implementation");
    }
}

// Client class
public class Client {
    public static void main(String[] args) {
        AbstractClass abstractClass1 = new ConcreteClass1();
        abstractClass1.templateMethod();

        System.out.println();

        AbstractClass abstractClass2 = new ConcreteClass2();
        abstractClass2.templateMethod();
    }
}
