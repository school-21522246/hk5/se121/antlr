package com.se121.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

// Visitor interface
interface Visitor {
    void visit(ElementA elementA);
    void visit(ElementB elementB);
}

// ConcreteVisitor class
class ConcreteVisitor implements Visitor {
    @Override
    public void visit(ElementA elementA) {
        System.out.println("Visiting ElementA and performing operation");
    }

    @Override
    public void visit(ElementB elementB) {
        System.out.println("Visiting ElementB and performing operation");
    }
}

// Element interface
interface Element {
    void accept(Visitor Visitor);
}

// ConcreteElementA class
class ElementA implements Element {
    @Override
    public void accept(Visitor Visitor) {
        Visitor.visit(this);
    }
}

// ConcreteElementB class
class ElementB implements Element {
    @Override
    public void accept(Visitor Visitor) {
        Visitor.visit(this);
    }
}

// ObjectStructure class
class ObjectStructure {
    private List<Element> elements = new ArrayList<>();

    public void addElement(Element element) {
        elements.add(element);
    }

    public void acceptVisitor(Visitor Visitor) {
        for (Element element : elements) {
            element.accept(Visitor);
        }
    }
}

// Client class
public class Client {
    public static void main(String[] args) {
        ObjectStructure objectStructure = new ObjectStructure();
        objectStructure.addElement(new ElementA());
        objectStructure.addElement(new ElementB());

        Visitor Visitor = new ConcreteVisitor();
        objectStructure.acceptVisitor(Visitor);
    }
}
