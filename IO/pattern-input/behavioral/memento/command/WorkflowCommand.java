package com.se121.behavioral.memento.command;

public interface WorkflowCommand {

    void execute();

    void undo();
}
