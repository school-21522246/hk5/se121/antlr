package com.se121.behavioral.chain.of.responsibility;

//This represents a handler in chain of responsibility
public interface LeaveApprover {
	void processLeaveApplication(LeaveApplication application);
	String getApproverRole();
}
