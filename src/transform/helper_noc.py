from src.grammars.GrammarData import GrammarData
from src.transform.TransformedData import dict_tran_data, dict_gram_data

def transform_noc(list_data: list[GrammarData]):
    for data in list_data:
        if data.class_or_interface == 'interface':
            transform_noc_interface(data)
        else:
            transform_noc_class(data)

##### CLASS #####
def transform_noc_class(data: GrammarData):

    parent = str(data.extends)
    if parent != '':
        parent_tranData = dict_tran_data.get(parent)
        if parent_tranData:
            parent_tranData.noc += 1
            transform_noc_class(dict_gram_data[parent])

    if len(data.implements) > 0:
        for implement in data.implements:
            parent_tranData = dict_tran_data.get(implement)
            if parent_tranData:
                parent_tranData.noc += 1
    pass

##### INTERFACE #####
def transform_noc_interface(data: GrammarData):
    if len(data.extends) > 0:
        for extend in data.extends:
            tranData = dict_tran_data.get(extend)
            if tranData:
                tranData.noc += 1
                transform_noc_interface(dict_gram_data[extend])
    pass

