from src.grammars.GrammarData import GrammarData

class TransformedData:
    def __init__(self, name: str, class_or_interface: str):
        self.name: str = name
        self.dit: int = 0
        self.noc: int = 0
        self.cbo: int = 0
        self.class_or_interface: str = class_or_interface

dict_gram_data: dict[str, GrammarData] = dict()
dict_tran_data: dict[str, TransformedData] = dict()

# name            BaseInterface1
# dit             0
# noc             2
# cbo             0
# is_interface    True
# -----------------
# name            BaseInterface2
# dit             1
# noc             1
# cbo             0
# is_interface    True
# -----------------
# name            BaseInterface3
# dit             0
# noc             1
# cbo             0
# is_interface    True
# -----------------
# name            Product
# dit             3
# noc             0
# cbo             0
# is_interface    True
# -----------------
# name            ConcreteProduct
# dit             0
# noc             0
# cbo             0
# is_interface    False
# -----------------
# name            Creator
# dit             0
# noc             0
# cbo             2
# is_interface    True
# -----------------
# name            ConcreteCreator
# dit             0
# noc             0
# cbo             1
# is_interface    False
# -----------------
# name            Client
# dit             0
# noc             0
# cbo             2
# is_interface    False
# -----------------
# name            Diagram
# dit             0
# noc             0
# cbo             1
# is_interface    False
# -----------------
# name            FistClass
# dit             0
# noc             3
# cbo             0
# is_interface    False
# -----------------
# name            SecondClass
# dit             1
# noc             2
# cbo             0
# is_interface    False
# -----------------
# name            ThirdClass
# dit             2
# noc             1
# cbo             0
# is_interface    False
# -----------------
# name            ForthClass
# dit             3
# noc             0
# cbo             0
# is_interface    False
