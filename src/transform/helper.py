from src.grammars.GrammarData import GrammarData
from src.transform.TransformedData import TransformedData, dict_tran_data, dict_gram_data
from src.transform.helper_dit import transform_dit
from src.transform.helper_noc import transform_noc
from src.transform.helper_cbo import transform_cbo

def transform(list_data: list[GrammarData]) -> list[TransformedData]:
    # TODO: clear others project data
    dict_gram_data.clear()
    dict_tran_data.clear()

    # TODO: load new data
    for data in list_data:
        dict_gram_data[data.name] = data
        dict_tran_data[data.name] = TransformedData(data.name, data.class_or_interface)

    # TODO: transform data
    transform_dit(list_data)
    transform_noc(list_data)
    transform_cbo(list_data)

    return list(dict_tran_data.values())

