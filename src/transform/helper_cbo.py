from src.grammars.GrammarData import GrammarData
from src.transform.TransformedData import dict_tran_data

def transform_cbo(list_data: list[GrammarData]):
    for data in list_data:
        dict_tran_data[data.name].cbo = len(data.dependencies)

    for data in list_data:
        for depend in data.dependencies:
            depend_tran_data = dict_tran_data.get(depend)
            if depend_tran_data:
                depend_tran_data.cbo += 1

