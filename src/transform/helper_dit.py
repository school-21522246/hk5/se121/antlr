from src.grammars.GrammarData import GrammarData
from src.transform.TransformedData import dict_tran_data, dict_gram_data

def transform_dit(list_data: list[GrammarData]):
    for data in list_data:
        if data.class_or_interface == 'interface':
            transform_dit_interface(data)
        else:
            transform_dit_class(data)

##### CLASS #####
def transform_dit_class(data: GrammarData):

    extends = str(data.extends)
    if extends != '':
        if dict_tran_data[data.name].dit != 0:
            return
        grammarData = dict_gram_data.get(extends)
        if grammarData:
            transform_dit_class(grammarData)
            dict_tran_data[data.name].dit = 1 + dict_tran_data[extends].dit

    if len(data.implements) > 0:
        for implement in data.implements:
            grammarData = dict_gram_data.get(implement)
            if grammarData is None:
                continue
            transform_dit_interface(grammarData)
            dict_tran_data[data.name].dit += 1 + dict_tran_data[implement].dit

##### INTERFACE #####
def transform_dit_interface(data: GrammarData):
    if len(data.extends) > 0:
        if dict_tran_data[data.name].dit != 0:
            return

        total = 0
        for extend in data.extends:
            transform_dit_interface(dict_gram_data[extend])
            # dict_tran_data[data.name].dit = max(dict_tran_data[data.name].dit, 1 + dict_tran_data[extend].dit)
            total += 1 + dict_tran_data[extend].dit

        dict_tran_data[data.name].dit = total

