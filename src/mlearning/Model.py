from sklearn.metrics import accuracy_score
from src.mlearning.helper_project import load_projects
from src.mlearning.helper_model import (
    create_model,
    find_best_model,
    extract_data,
    get_target_encoder,
)

import os
import pandas as pd
import pickle

class Model:
    def __init__(self, outDir: str = 'IO/output', model_path: str = 'model.pkl') -> None:
        self.outDir = outDir
        os.makedirs(outDir) if not os.path.exists(outDir) else None
        self.model_path = model_path
        self.target_encoder = get_target_encoder()


    def load_project(self, inDir: str = 'IO/input') -> str:
        if not os.path.exists(inDir):
            raise FileNotFoundError(f'No such file or directory: {inDir}')

        basename = os.path.basename(inDir)
        if basename == 'input' or basename == os.path.basename(self.outDir):
            outDir = self.outDir
            csv_name = os.path.join(outDir, 'transformed_projects.csv')
            json_name = os.path.join(outDir, 'parsed_projects.json')
        else:
            outDir = os.path.join(self.outDir, basename)
            os.makedirs(outDir) if not os.path.exists(outDir) else None
            csv_name = os.path.join(outDir, 'transformed_' + basename + '.csv')
            json_name = os.path.join(outDir, 'parsed_' + basename + '.json')

        dict_projects, list_raw_projects = load_projects(inDir)

        pd.DataFrame.from_dict(dict_projects).to_csv(csv_name, index=False)
        pd.DataFrame(list_raw_projects).to_json(json_name, orient='records')
        print("Parsing is done")
        print(os.path.basename(json_name) + " is created")
        print(os.path.basename(csv_name) + " is created")
        print()
        return csv_name


    def save_model(self) -> None:
        with open(self.model_path,'wb') as f:
            pickle.dump(self.pipeline,f)
        pass
    def load_model(self) -> None:
        with open(self.model_path, 'rb') as f:
            self.pipeline = pickle.load(f)
        pass


    def train(self, csv_name: str) -> None:
        create_model(find_best=False)

        X_train, y_train, _ = extract_data(csv_name)
        y_train_onehot = self.target_encoder.transform(y_train)

        self.pipeline.fit(X_train, y_train_onehot)
        self.save_model()
        pass


    def test(self, csv_name: str) -> None:
        self.load_model()

        X_test, y_test, _ = extract_data(csv_name)
        y_test_onehot = self.target_encoder.transform(y_test)

        y_pred_onehot = self.pipeline.predict(X_test)
        accuracy = accuracy_score(y_test_onehot, y_pred_onehot)
        print(f'Test Accuracy: {accuracy}')
        pass


    def find_best_model(self, csv_name: str) -> None:
        X_train, y_train, _ = extract_data(csv_name)
        y_train_onehot = self.target_encoder.transform(y_train)

        self.pipeline = find_best_model(X_train, y_train_onehot)
        self.save_model()
        pass


    def predict(self, csv_name: str) -> None:
        self.load_model()

        X, _, data = extract_data(csv_name)
        y_pred_onehot = self.pipeline.predict(X)

        y_pred = self.target_encoder.inverse_transform(y_pred_onehot)
        y_pred = y_pred.reshape(-1)
        data['detect'] = [y if y else 'NotRelated' for y in y_pred]
        print('Prediction is done.')

        dirname = os.path.dirname(csv_name)

        result_json = os.path.join(dirname, 'result.json')
        data.to_json(result_json, orient='records')
        print(f'Json Prediction result is saved in {result_json}')

        result_csv = os.path.join(dirname, 'result.csv')
        data.to_csv(result_csv, index=False)
        print(f'Csv Prediction result is saved in {result_csv}')
        pass

    def run(self, inDir: str = 'IO/input') -> None:
        numDirs = 0
        for i in os.listdir(inDir):
            if os.path.isfile(os.path.join(inDir, i)):
                raise Exception('Only directory is allowed in input directory.')
            elif os.path.isdir(os.path.join(inDir, i)):
                numDirs += 1
        if numDirs == 0:
            raise Exception(f'Empty input: {inDir}')
        csv_name = self.load_project(inDir)
        self.predict(csv_name)
        pass



