from sklearn.neural_network import MLPClassifier
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import (
    StandardScaler,
    OneHotEncoder, 
)

import pandas as pd


def extract_data(csv_name: str) -> tuple:
    raw_data = pd.read_csv(csv_name)
    X_test = raw_data[['dit', 'noc', 'cbo', 'class_or_interface']].values
    y_test = raw_data['detect'].values.reshape(-1, 1)
    return X_test, y_test, raw_data


def create_model(find_best: bool=False) -> Pipeline:
    if find_best:
        model = MLPClassifier(hidden_layer_sizes=(64, 32), max_iter=1000, random_state=42)
    else:
        model = MLPClassifier(
                activation='logistic', # 'relu', 'tanh', 'logistic (sigmoid)
                # activation='relu',
                # activation='tanh',
                alpha=0.0001,
                hidden_layer_sizes=(64, 32),
                max_iter=1000,
                random_state=42
            )

    numeric_features = [0, 1, 2] # ['dit', 'noc', 'cbo']
    categorical_features = [3] # ['class_or_interface']

    preprocessor = ColumnTransformer(
        transformers=[
            ('scaler', StandardScaler(), numeric_features),
            ('encoder', OneHotEncoder(), categorical_features)
        ])
# Create a pipeline with preprocessing and the classifier
    return Pipeline([
            ('preprocessor', preprocessor),
            ('classifier', model)
        ])


def find_best_model(X_train, y_train_onehot):
    pipeline = create_model(find_best=True)

    param_grid = {
        'classifier__hidden_layer_sizes': [ (64, 32), (32, 32), (128, 64, 32) ],
        'classifier__activation': ['relu', 'tanh', 'logistic'],
        'classifier__alpha': [0.0001, 0.001, 0.01]
    }

    grid_search = GridSearchCV(pipeline, param_grid, cv=5, verbose=2)
    grid_search.fit(X_train, y_train_onehot)

    print(f'Best parameters: {grid_search.best_params_}')
    print(f'Best accuracy: {grid_search.best_score_}')
    
    return grid_search.best_estimator_


def get_target_encoder():
    target_encoder = OneHotEncoder(handle_unknown='ignore')
    raw_data = pd.read_csv('src/mlearning/encoding.csv')
    encoding = raw_data['encoding'].values.reshape(-1, 1)
    target_encoder.fit(encoding)
    return target_encoder


def convert_to_json(data: list) -> list:
    json_data = []
    for d in data:
        json_data.append({
            'name': d.name,
            'dit': d.dit,
            'noc': d.noc,
            'cbo': d.cbo,
            'class_or_interface': d.class_or_interface
        })

    return json_dataon
