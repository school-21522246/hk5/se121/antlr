import os
from antlr4 import (
    CommonTokenStream,
    InputStream,
    ParseTreeWalker,
)
from src.grammars.java.JavaLexer import JavaLexer
from src.grammars.java.JavaParser import JavaParser
from src.grammars.MyJavaListerner import MyJavaListener
from src.transform.TransformedData import TransformedData
from src.transform.helper import transform


def load_projects(inDir) -> tuple:
    dict_projects = dict()
    dict_projects["proj_name"] = []
    dict_projects["class_name"] = []
    dict_projects["dit"] = []
    dict_projects["noc"] = []
    dict_projects["cbo"] = []
    dict_projects["class_or_interface"] = []
    dict_projects["detect"] = []

    list_raw_data = list()

    for proj in os.listdir(inDir):
        proj_path = os.path.join(inDir, proj)
        transformed_data, list_data = load_project(proj_path)

        list_raw_data.append(list_data)
        map_list_to_dict(dict_projects, proj, transformed_data)

    return dict_projects, list_raw_data


def load_project(proj_path) -> tuple:
    list_tree: list[JavaParser.CompilationUnitContext] = []
    for root, dirs, files in os.walk(proj_path):
        for file in files:
            if file.endswith(".java"):
                list_tree.append(get_tree(os.path.join(root, file)))

    tree = list_tree[0]
    for i in range(1, len(list_tree)):
        tree.addChild(list_tree[i])

    transformed_data, list_data = get_data(tree)
    # print('---------------------------------------------------')
    # print('---------------------------------------------------')
    return transformed_data, list_data


def get_data(tree: JavaParser.CompilationUnitContext) -> tuple:
    listener = MyJavaListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)

    data = transform(listener.list_data)
    # for d in data:
    #     print(f'name                {d.name}')
    #     print(f'dit                 {d.dit}')
    #     print(f'noc                 {d.noc}')
    #     print(f'cbo                 {d.cbo}')
    #     print(f'class_or_interface  {d.class_or_interface}')
    #     print('-----------------')

    return data, listener.list_data


def get_tree(java_path: str) -> JavaParser.CompilationUnitContext:
    code = open(java_path, "r").read()
    codeStream = InputStream(code)

    lexer = JavaLexer(codeStream)
    token_stream = CommonTokenStream(lexer)
    parser = JavaParser(token_stream)
    tree = parser.compilationUnit()

    return tree


def map_list_to_dict(dict_projects: dict, proj_name, list_data: list[TransformedData]):
    for data in list_data:
        dict_projects["proj_name"].append(proj_name)
        dict_projects["class_name"].append(data.name)
        dict_projects["dit"].append(data.dit)
        dict_projects["noc"].append(data.noc)
        dict_projects["cbo"].append(data.cbo)
        dict_projects["class_or_interface"].append(data.class_or_interface)
        dict_projects["detect"].append("Common")
    pass
