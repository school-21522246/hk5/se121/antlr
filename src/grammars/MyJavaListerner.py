from antlr4 import ParseTreeListener
from src.grammars.GrammarData import GrammarData
from src.grammars.helper import (
    get_extend_class,
    get_implement_interfaces,
    get_class_dependencies,
    get_extend_interfaces,
    get_identifier,
    get_interface_dependencies,
)

class MyJavaListener(ParseTreeListener):
    def __init__(self):
        self.list_data: list[GrammarData] = list()

    def enterInterfaceDeclaration(self, ctx):
        interface_name = get_identifier(ctx)
        ### DIT, NOC
        extend_interfaces = get_extend_interfaces(ctx)
        ### CBO
        dependencies = get_interface_dependencies(ctx)

        grammarData = self.refactored_data(interface_name, extend_interfaces, [], dependencies, 'interface')
        self.list_data.append(grammarData)

    def enterClassDeclaration(self, ctx):
        class_name = get_identifier(ctx)
        ### DIT, NOC
        extend_class = get_extend_class(ctx)
        implement_interfaces = get_implement_interfaces(ctx)
        ### CBO
        dependencies = get_class_dependencies(ctx)

        grammarData = self.refactored_data(class_name, extend_class, implement_interfaces, dependencies, 'class')
        self.list_data.append(grammarData)

    def refactored_data(self, name: str, extends: str | list[str], implements: list[str], dependencies: list[str], class_or_interface: str):
        name = self.remove_generic(name)
        extends = list(map(self.remove_generic, extends)) if isinstance(extends, list) else self.remove_generic(extends)
        implements = list(map(self.remove_generic, implements))
        dependencies = list(map(self.remove_generic, dependencies))
        return GrammarData(name, extends, implements, dependencies, class_or_interface)

    def remove_generic(self, name: str):
        idx = name.find('<')
        return name[:idx] if idx != -1 else name

