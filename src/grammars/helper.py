from src.grammars.java.JavaParser import JavaParser

#################### Class helper functions ###########################
def get_extend_class(ctx):
    extended_class = ''
    if ctx.EXTENDS():
        extended_class = ctx.typeType().getText()

    return extended_class

def get_implement_interfaces(ctx):
    implemented_interfaces = []
    if ctx.IMPLEMENTS():
        implemented_interfaces = extract_typeList(ctx)
    return implemented_interfaces

def get_class_dependencies(ctx):
    dependencies = set()

    for body in ctx.classBody().classBodyDeclaration():
        member = body.memberDeclaration()
        if member is None:
            continue

        fieldDecl = member.fieldDeclaration()
        if fieldDecl is not None:
            field_type = fieldDecl.typeType().getText()
            dependencies.add(field_type)

        methodDecl = member.methodDeclaration()
        if methodDecl is not None:
            method_type = methodDecl.typeTypeOrVoid().getText()
            dependencies.add(method_type)

            block = methodDecl.methodBody().block()
            if block is None:
                continue
            blockStatments = block.blockStatement()
            for blockStatment in blockStatments:
                localVariableDecl = blockStatment.localVariableDeclaration()
                if localVariableDecl is not None:
                    type_type = localVariableDecl.typeType()
                    if type_type is not None:
                        variable_type = type_type.getText()
                        dependencies.add(variable_type)

    return drop_system_types(list(dependencies))




#################### Interface helper functions ###########################
def get_extend_interfaces(ctx):
    extended_interfaces = []
    if ctx.EXTENDS():
        extended_interfaces = extract_typeList(ctx)
    return extended_interfaces

def get_interface_dependencies(ctx):
    dependencies = set()

    for body in ctx.interfaceBody().interfaceBodyDeclaration():
        member = body.interfaceMemberDeclaration()

        methodDecl = member.interfaceMethodDeclaration()
        if methodDecl is not None:
            commonBodyDecl = methodDecl.interfaceCommonBodyDeclaration()
            if commonBodyDecl is not None:
                method_type = commonBodyDecl.typeTypeOrVoid().getText()
                dependencies.add(method_type)

    return drop_system_types(list(dependencies))



#################### Common helper functions ###########################
def get_identifier(ctx):
    return ctx.identifier().getText()

def extract_typeList(ctx):
    result = []
    list_type = ctx.typeList()
    for i in list_type:
        for j in i.typeType():
            result.append(j.getText())
    return result

def drop_system_types(dependencies: list[str]):
    # return [dep for dep in dependencies if (dep.capitalize() not in JavaParser.symbolicNames) and ((dep + "_literal").capitalize() not in JavaParser.symbolicNames)]
    for dep in dependencies:
        if dep.upper() in JavaParser.symbolicNames:
            dependencies.remove(dep)
        elif (dep + "_literal").upper() in JavaParser.symbolicNames:
            dependencies.remove(dep)

    return dependencies

