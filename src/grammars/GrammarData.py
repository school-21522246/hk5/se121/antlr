
class GrammarData:
    def __init__(self, name: str, extends: str | list, implements: list, dependencies: list, class_or_interface: str):
        self.name: str = name
        self.extends: str | list = extends
        self.implements: list = implements
        self.dependencies: list = dependencies
        self.class_or_interface: str = class_or_interface



# name            BaseInterface1
# extends         []
# implements      []
# dependencies    []
# is_interface    True
# -----------------
# name            BaseInterface2
# extends         ['BaseInterface1']
# implements      []
# dependencies    []
# is_interface    True
# -----------------
# name            BaseInterface3
# extends         []
# implements      []
# dependencies    []
# is_interface    True
# -----------------
# name            Product
# extends         ['BaseInterface2', 'BaseInterface3']
# implements      []
# dependencies    []
# is_interface    True
# -----------------
# name            ConcreteProduct
# extends         
# implements      ['Product']
# dependencies    []
# is_interface    False
# -----------------
# name            Creator
# extends         []
# implements      []
# dependencies    ['Product', 'BaseInterface1']
# is_interface    True
# -----------------
# name            ConcreteCreator
# extends         
# implements      ['Creator']
# dependencies    ['Product']
# is_interface    False
# -----------------
# name            Client
# extends         
# implements      []
# dependencies    ['Product', 'Creator']
# is_interface    False
# -----------------
# name            Diagram
# extends         
# implements      []
# dependencies    ['Creator']
# is_interface    False
# -----------------
# name            FistClass
# extends         
# implements      []
# dependencies    []
# is_interface    False
# -----------------
# name            SecondClass
# extends         FistClass
# implements      []
# dependencies    []
# is_interface    False
# -----------------
# name            ThirdClass
# extends         SecondClass
# implements      []
# dependencies    []
# is_interface    False
# -----------------
# name            ForthClass
# extends         ThirdClass
# implements      []
# dependencies    []
# is_interface    False
