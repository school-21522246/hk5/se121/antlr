from src.mlearning.Model import Model

def main():
    # name = 'creational'
    # name = 'structural'
    # name = 'behavioral'

    # inDir = f"IO/input/{name}"
    # Model(inDir)

    # model = Model(None)
    # model.find_best_model(f"IO/output/{name}/{name}.train.csv")
    # model.train(f"IO/output/{name}/{name}.train.csv")
    # model.test(f"IO/output/{name}/{name}.test.csv")

    # model = Model(None)
    # model.find_best_model("IO/output/train-total.csv")
    # model.train("IO/output/train-total.csv")
    # model.test("IO/output/test-total.csv")

    model = Model()
    model.run()

    pass

if __name__ == "__main__":
    main()
